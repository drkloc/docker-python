FROM python:2.7.14-jessie
MAINTAINER Teo Sibileau

# Update packages and install software
RUN apt-get update \
    && apt-get -y install git python-dev python-pip libfontconfig libssl-dev build-essential libssl-dev libffi-dev python-lxml libxml2-dev libxslt1-dev libffi-dev software-properties-common

RUN curl -sL https://deb.nodesource.com/setup_7.x -o nodesource_setup.sh \
    && bash nodesource_setup.sh \
    && apt-get update \
    && apt-get -y install nodejs

ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
